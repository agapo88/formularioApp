package curso.umg.gt.formularioapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Toast;

public class Preferencias extends AppCompatActivity {
    private Button btnEnviar;
    private RadioButton rbJS,rbJN,rbSS,rbSN,rbAS,rbAN;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preferencias);
        // ANDROID
        rbAS = (RadioButton) findViewById( R.id.rbtnAS );
        rbAN = (RadioButton) findViewById( R.id.rbtnAN );
        // SPRING
        rbSS = (RadioButton) findViewById( R.id.rbtnSS );
        rbSN = (RadioButton) findViewById( R.id.rbtnSN );
        // JAVA
        rbJS = (RadioButton) findViewById( R.id.rbtnJS );
        rbJN = (RadioButton) findViewById( R.id.rbtnJN );

    }

    public void enviarPreferencias(View v){

        if( ( !rbAS.isChecked() && !rbAN.isChecked() ) && ( !rbSS.isChecked() && !rbSN.isChecked() ) && ( !rbJS.isChecked() && !rbJN.isChecked() ) ){
            Toast notificacion= Toast.makeText(this,"No ha seleccionado ninguna opcion", Toast.LENGTH_SHORT);
            notificacion.show();
        }
        else if( ( !rbAS.isChecked() && !rbAN.isChecked() ) ){
            Toast notificacion= Toast.makeText(this,"No ha seleccionado la preferenia de ANDROID", Toast.LENGTH_SHORT);
            notificacion.show();
        }
        else if( ( !rbJS.isChecked() && !rbJN.isChecked() ) ){
            Toast notificacion= Toast.makeText(this,"No ha seleccionado la preferenia de JAVA", Toast.LENGTH_SHORT);
            notificacion.show();
        }
        else if( ( !rbSS.isChecked() && !rbSN.isChecked() ) ){
            Toast notificacion= Toast.makeText(this,"No ha seleccionado la preferenia de SPRING", Toast.LENGTH_SHORT);
            notificacion.show();
        }else{

            String preferencias = "MIS PREFERENCIAS SON: /";

            if( rbAS.isChecked() )
                preferencias = preferencias.concat( " ANDROID /" );
            if( rbJS.isChecked() )
                preferencias = preferencias.concat( " JAVA /" );
            if( rbSS.isChecked() ) {
                preferencias = preferencias.concat( " SPRING /");
            }

            Toast notificacion= Toast.makeText(this, preferencias.toString() , Toast.LENGTH_SHORT);
            notificacion.show();
        }


    }
}
