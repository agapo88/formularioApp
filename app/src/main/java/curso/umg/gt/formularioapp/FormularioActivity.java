package curso.umg.gt.formularioapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class FormularioActivity extends AppCompatActivity {

    private EditText etNombre, etApellidos, etPassword, etPassword1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario);

        etNombre = (EditText) findViewById( R.id.editNombre );
        etApellidos = (EditText) findViewById( R.id.editApellidos );
        etPassword = (EditText) findViewById( R.id.editPassword );
        etPassword1 = (EditText) findViewById( R.id.editPassword1 );

    }

    public void accederLogin(View view){
        Intent i = null;

        String nombre, apellidos, password, password1;

        nombre      = etNombre.getText().toString();
        apellidos   = etApellidos.getText().toString();
        password    = etPassword.getText().toString();
        password1   = etPassword1.getText().toString();

        if( nombre.equals("") && apellidos.equals("") && password.equals("") && password1.equals("") ) {
            Toast notificacion= Toast.makeText(this,"Ingrese todos los campos del formulario", Toast.LENGTH_SHORT);
            notificacion.show();
        }else if( !password.equals( password1 )  ){
            Toast notificacion= Toast.makeText(this,"Las claves no coinciden", Toast.LENGTH_SHORT);
            notificacion.show();
        }
        else{
            i = new Intent(this, Preferencias.class);
            startActivity(i);
        }

    }

    // LIMPIAR CAMPOS
    public void cancelarLogin( View view ){
        etNombre.setText("");
        etApellidos.setText("");
        etPassword.setText("");
        etPassword1.setText("");

    }
}
